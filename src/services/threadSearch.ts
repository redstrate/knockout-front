/* eslint-disable import/prefer-default-export */
import { ThreadSearchRequest, ThreadSearchResponse } from 'knockout-schema';
import { authPost } from './common';

export interface ThreadSearchParams {
  title: string;
  subforumId?: number;
  tagIds?: number[];
  userId?: number;
  sortBy?: 'created_at' | 'updated_at' | 'relevance';
  sortOrder?: 'desc' | 'asc';
  page?: number;
}

export const threadSearch = async ({
  title,
  subforumId,
  tagIds,
  userId,
  sortBy = 'relevance',
  sortOrder = 'desc',
  page = 1,
}: ThreadSearchParams): Promise<ThreadSearchResponse> => {
  const body: ThreadSearchRequest = {
    title,
    sort_by: sortBy,
    sort_order: sortOrder,
    page,
  };

  if (subforumId) {
    body.subforum_id = subforumId;
  }

  if (tagIds) {
    body.tag_ids = tagIds;
  }

  if (userId) {
    body.user_id = userId;
  }

  try {
    const res = await authPost({
      url: `/v2/threadSearch`,
      data: body,
    });

    if (res) {
      return res.data as ThreadSearchResponse;
    }
  } catch {
    console.error('Could not search threads');
  }
  return { totalThreads: 0, threads: [] };
};
