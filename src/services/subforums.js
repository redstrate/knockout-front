import axios from 'axios';

import config from '../../config';

import { authGet } from './common';

export const getSubforumList = async (hideNsfw) => {
  const res = await axios.get(`${config.apiHost}/subforum${hideNsfw ? '?hideNsfw=1' : ''}`);

  const { data } = res;

  return data;
};

export const getSubforumWithThreads = async (subforumid, page = 1, hideNsfw = true) => {
  const user = localStorage.getItem('currentUser');

  // if logged in, get the subforum with auth so
  // subscribed threads are fetched
  if (user) {
    const res = await authGet({
      url: `/subforum/${subforumid}/${page}${hideNsfw ? '?hideNsfw=1' : ''}`,
    });

    const { data } = res;

    return data;
  }

  const res = await axios.get(
    `${config.apiHost}/subforum/${subforumid}/${page}${hideNsfw ? '?hideNsfw=1' : ''}`
  );

  const { data } = res;

  return data;
};
