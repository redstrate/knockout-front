import { authPost, authDelete } from './common';

export const createReadThreadRequest = async ({ threadId, lastSeen, previousLastSeen }) => {
  if (!threadId || !lastSeen) return { error: 'Invalid ReadThread' };

  if (new Date(lastSeen) > new Date(previousLastSeen)) {
    const requestBody = {
      lastSeen,
      threadId,
    };

    const response = await authPost({ url: '/readThreads', data: requestBody });

    return response;
  }

  return null;
};

export const deleteReadThread = async (threadId) => {
  if (!threadId) throw Error('Invalid thread id');

  const requestBody = {
    threadId,
  };

  const response = await authDelete({ url: '/readThreads', data: requestBody });

  return response;
};

export const getReadThreads = async () => {
  const user = localStorage.getItem('currentUser');

  if (!user) {
    return [];
  }
  try {
    const results = await authPost({ url: '/readThreads/list', data: {} });

    return results.data;
  } catch (err) {
    return [];
  }
};
