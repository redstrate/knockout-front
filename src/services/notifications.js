import { pushSmartNotification } from '../utils/notification';
import { authGet, authPut } from './common';

export const getNotifications = async () => {
  const response = await authGet({ url: '/v2/notifications' });
  return response.data;
};

export const markNotificationsAsRead = async (notificationIds) => {
  const user = localStorage.getItem('currentUser');

  if (!user || !notificationIds) {
    return [];
  }
  const results = await authPut({ url: '/v2/notifications', data: { notificationIds } });
  return results.data;
};

export const markNotificationAsRead = async (notificationId) =>
  markNotificationsAsRead([notificationId]);

export const markAllNotificationsAsRead = async () => {
  const user = localStorage.getItem('currentUser');

  if (!user) {
    return undefined;
  }
  await authPut({ url: '/v2/notifications/all' });
  return pushSmartNotification({ message: 'Marked all notifications as read.' });
};
