export const HEADER_UPDATE = 'HEADER_UPDATE';
export const THEME_UPDATE = 'THEME_UPDATE';
export const WIDTH_UPDATE = 'WIDTH_UPDATE';
export const MOTD_UPDATE = 'MOTD_UPDATE';

export function updateHeader(value) {
  return {
    type: HEADER_UPDATE,
    value,
  };
}

export function updateTheme(value) {
  return {
    type: THEME_UPDATE,
    value,
  };
}

export function updateWidth(value) {
  return {
    type: WIDTH_UPDATE,
    value,
  };
}

export function updateMotdDisplay(value) {
  return {
    type: MOTD_UPDATE,
    value,
  };
}
