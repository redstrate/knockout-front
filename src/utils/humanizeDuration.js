import dayjs from 'dayjs';

const humanizeDuration = (start, end) => {
  const startTime = dayjs(start);
  const endTime = dayjs(end);

  const hoursLength = endTime.diff(startTime) / 3600000;
  let diffType;

  if (hoursLength < 24) {
    diffType = 'hours';
  } else if (hoursLength >= 24 && hoursLength < 168) {
    diffType = 'days';
  } else if (hoursLength >= 168 && hoursLength < 744) {
    diffType = 'weeks';
  } else if (hoursLength >= 744 && hoursLength < 8760) {
    diffType = 'months';
  } else if (hoursLength < 87600) {
    diffType = 'years';
  } else {
    return 'forever';
  }

  const hourDifference = endTime.diff(startTime, diffType);
  if (hourDifference === 1) {
    diffType = diffType.substring(0, diffType.length - 1);
  }

  return `${hourDifference} ${diffType}`;
};

export default humanizeDuration;
