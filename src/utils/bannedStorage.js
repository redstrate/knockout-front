export const BAN_INFO_STORAGE_KEY = 'banInformation-v2';

export const loadBannedMessageFromStorage = () => {
  const banInformation = JSON.parse(localStorage.getItem(BAN_INFO_STORAGE_KEY));

  if (banInformation) {
    return banInformation;
  }
  return null;
};

export const saveBannedMessageToStorage = (data) => {
  const { banMessage, expiresAt, postId } = data;
  const banInformation = JSON.stringify({ banMessage, expiresAt, postId });

  localStorage.setItem(BAN_INFO_STORAGE_KEY, banInformation);
};

export const clearBannedMessageFromStorage = () => {
  localStorage.removeItem(BAN_INFO_STORAGE_KEY);
};
