import { css } from 'styled-components';
import { ThemeHoverFilter, ThemeHoverShadow } from '../../utils/ThemeNew';

export const buttonHover = css`
  cursor: pointer;
  transition: background 85ms ease-in-out, box-shadow 85ms ease-in-out;

  &:hover {
    background: #ec3737;
    box-shadow: inset 0px 0px 10px 0px #ff5959;
  }
`;

export const buttonHoverGray = css`
  cursor: pointer;
  transition: filter 85ms ease-in-out, box-shadow 85ms ease-in-out;

  &:hover {
    filter: ${ThemeHoverFilter};
    box-shadow: ${ThemeHoverShadow};
  }
`;

export const buttonHoverBrightness = css`
  cursor: pointer;
  transition: filter 85ms ease-in-out, box-shadow 85ms ease-in-out;

  &:hover,
  &:focus {
    filter: ${ThemeHoverFilter};
    box-shadow: ${ThemeHoverShadow};
  }
`;

export const MobileMediaQuery = `@media (max-width: 700px)`;
export const TabletMediaQuery = `@media (max-width: 900px)`;
export const DesktopMediaQuery = `@media (min-width: 1200px)`;
export const SmallDesktopMediaQuery = `@media (max-width: 1200px)`;
