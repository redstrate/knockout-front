import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import userRoles, { goldMemberFallbackColor } from './userRoles';
import { isDeletedUser } from '../../utils/deletedUser';

export const StyledUserRoleWrapper = styled.span`
  color: ${(props) => props.userRole.color};

  ${(props) => props.userRole.extraStyle};

  ${(props) => isDeletedUser(props.username) && 'color: #999999;'}

  overflow: hidden;
  text-overflow: ellipsis;

  ${(props) =>
    props.userRole.color === goldMemberFallbackColor &&
    `.role-icon {
    color: ${props.userRole.color};
  }`}
`;

/**
 * @param {{ user: { role: { code: string }, isBanned: boolean }, children: React.DOMElement[] | string }} props
 */
const UserRoleWrapper = (props) => {
  const userRoleCode = props.user?.role?.code || 'basic-user';
  const userRole = userRoles[userRoleCode] ? userRoles[userRoleCode] : userRoles['basic-user'];
  const isBanned = (props.user && props.user.isBanned) || false;
  const title = `${props.user.username} - ${userRole.name}`;

  return (
    <StyledUserRoleWrapper
      className={`user-role-wrapper-component ${props.className}`}
      userRole={userRole}
      title={title}
      isBanned={isBanned}
      username={props.user.username}
    >
      {props.children}
    </StyledUserRoleWrapper>
  );
};

UserRoleWrapper.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    isBanned: PropTypes.bool,
    role: PropTypes.shape({
      code: PropTypes.string,
    }),
  }),
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};
UserRoleWrapper.defaultProps = {
  user: { isBanned: false, role: { code: 'basic-user' } },
  className: '',
};

export default UserRoleWrapper;
