import dayjs from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import PostContext from '../../Post/PostContext';

export default (spoiler) => {
  const { user, postEdited } = useContext(PostContext);
  const newUser = dayjs(user.createdAt).add(2, 'week').isAfter(dayjs());
  const lowPostCount = user.posts < 15;
  const limitedUser =
    (newUser || lowPostCount) && dayjs(postEdited).add(18, 'hour').isAfter(dayjs());
  const [spoilered, setSpoilered] = useState(spoiler || limitedUser);

  useEffect(() => {
    setSpoilered(spoiler || limitedUser);
  }, [user]);

  const reveal = (setModalOpen) => {
    if (limitedUser) {
      setModalOpen(true);
    } else {
      setSpoilered(false);
    }
  };

  return [spoilered, setSpoilered, reveal, limitedUser];
};
