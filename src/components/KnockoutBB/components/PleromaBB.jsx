/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';

export const getPleromaId = (src) => {
  const idRegex = /([^/]+$)/;

  const id = idRegex.exec(src);

  return id[0];
};

export const getInstance = (src) => {
  const instanceRegex = /([a-z0-9|-]+\.)*[a-z0-9|-]+\.[a-z]+/;

  const instance = instanceRegex.exec(src);

  return instance[0];
};

const PleromaBB = ({ href }) => {
  const id = getPleromaId(href);
  const instance = getInstance(href);

  const newHref = `https://${instance}/embed/${id}`;
  return <iframe title="Pleroma Embed" src={newHref} width="400" />;
};
PleromaBB.propTypes = {
  href: PropTypes.string.isRequired,
};

export default PleromaBB;
