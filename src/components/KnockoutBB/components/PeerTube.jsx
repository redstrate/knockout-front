/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';

// example url: https://tube.ryne.moe/w/iczDZPUqGCz2rKvJ6CG87z
export const getVideoId = (src) => {
  const idRegex = /([^/]+$)/;

  const id = idRegex.exec(src);

  return id[0];
};

export const getInstance = (src) => {
  const instanceRegex = /([a-z0-9|-]+\.)*[a-z0-9|-]+\.[a-z]+/;

  const instance = instanceRegex.exec(src);

  return instance[0];
};

const PeerTubeBB = ({ href }) => {
  const instance = getInstance(href);
  const id = getVideoId(href);

  const newHref = `https://${instance}/videos/embed/${id}`;
  return (
    <iframe
      title="PeerTube Embed"
      src={newHref}
      frameBorder="0"
      allowFullScreen=""
      sandbox="allow-same-origin allow-scripts allow-popups"
    />
  );
};
PeerTubeBB.propTypes = {
  href: PropTypes.string.isRequired,
};

export default PeerTubeBB;
