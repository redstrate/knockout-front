/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';

const MastodonBB = ({ href }) => {
  const newHref = `${href}/embed`;
  return <iframe title="Mastodon Embed" src={newHref} width="400" />;
};
MastodonBB.propTypes = {
  href: PropTypes.string.isRequired,
};

export default MastodonBB;
