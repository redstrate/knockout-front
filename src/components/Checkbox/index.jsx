import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export const CheckboxStates = {
  Unchecked: 'Unchecked',
  Checked: 'Checked',
  Indeterminate: 'Indeterminate',
};

const CheckboxWrapper = styled.label`
  input,
  span {
    user-select: none;
  }
`;

/**
 * A checkbox component that supports an indeterminate state.
 * @param {props} props
 */
export const Checkbox = ({ state, readOnly, onChange, label, className }) => {
  const ref = useRef();

  useEffect(() => {
    switch (state) {
      case CheckboxStates.Unchecked:
      case CheckboxStates.Checked: {
        ref.current.checked = state === CheckboxStates.Checked;
        ref.current.indeterminate = false;
        break;
      }
      case CheckboxStates.Indeterminate: {
        ref.current.checked = false;
        ref.current.indeterminate = true;
        break;
      }
      default: {
        throw new Error('Expected a value from CheckboxStates');
      }
    }
  });

  return (
    <CheckboxWrapper>
      <input
        className={className}
        ref={ref}
        type="checkbox"
        onChange={onChange}
        readOnly={readOnly || false}
      />
      {label !== '' && <span>{label}</span>}
    </CheckboxWrapper>
  );
};

Checkbox.propTypes = {
  state: PropTypes.oneOf(Object.values(CheckboxStates)).isRequired,
  readOnly: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Checkbox.defaultProps = {
  readOnly: false,
  className: undefined,
};
