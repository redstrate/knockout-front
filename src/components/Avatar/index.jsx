import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import config from '../../../config';

export const Avatar = styled.img`
  width: auto;
  height: 100%;
  max-height: 40px;
`;

const UserAvatar = ({ src, alt, className }) => {
  let url = `${config.cdnHost}/image/${src}`;
  if (!src || src.length === 0) {
    url = `${config.cdnHost}/image/none.webp`;
  }
  return (
    <Avatar
      className={`${className} user-avatar`}
      src={url}
      title={alt || "User's Avatar"}
      alt=""
      onError={(e) => {
        e.target.onerror = null;
        e.target.src = `${config.cdnHost}/image/none.webp`;
      }}
    />
  );
};
UserAvatar.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  className: PropTypes.string,
};
UserAvatar.defaultProps = {
  alt: undefined,
  className: '',
};
export default UserAvatar;
