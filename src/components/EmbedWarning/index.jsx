import React from 'react';
import styled from 'styled-components';
import { saveEmbedSettingToStorage } from '../../utils/thirdPartyEmbedStorage';
import {
  ThemeMainBackgroundColor,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';

export const StyledEmbedWarning = styled.div`
  width: 100%;
  background: ${ThemeMainBackgroundColor};
  color: ${ThemeTextColor};
  text-align: center;
  max-width: 450px;
  padding: calc(${ThemeVerticalPadding} * 2);
  box-sizing: border-box;
  border-radius: 5px;
  margin-bottom: 15px;

  button {
    border: none;
    background: #ffc107;
    padding: 10px;
    border-radius: 3px;
    cursor: pointer;

    &:hover,
    &:focus {
      filter: brightness(1.1);
    }
  }
`;

const EmbedWarning = () => {
  return (
    <StyledEmbedWarning>
      <img src="https://cdn.knockout.chat/assets/warning.png" alt="Warning icon" />
      <p>
        <b>Watch out!</b>
      </p>
      <div>
        This 3rd party service embed might track your data or do something else - it&apos;s out of
        our hands.
      </div>
      <p>
        You can
        <b>permanently</b>
        unlock 3rd party embeds
        <b>at your own risk</b>
        by clicking thebutton below:
      </p>
      <button onClick={() => saveEmbedSettingToStorage()} type="button">
        Unlock embeds
      </button>
    </StyledEmbedWarning>
  );
};

export default EmbedWarning;
