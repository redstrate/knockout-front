import dayjs from 'dayjs';
import { PreviousUsername } from 'knockout-schema';
import React, { useRef } from 'react';
import styled from 'styled-components';
import useDropdownMenu, {
  DropdownMenuOpened,
} from '../../../components/Header/components/DropdownMenu';
import dateFormat from '../../../utils/dateFormat';
import { ThemeFontSizeLarge, ThemeTextColor } from '../../../utils/ThemeNew';

const DropdownButton = styled.button`
  position: relative;
  display: inline-block;
  margin-top: 4px;
  color: ${ThemeTextColor};
  font-size: ${ThemeFontSizeLarge};
  border: none;
  outline: none;
  background: none;
  cursor: pointer;
`;

const DropdownMenu = styled(DropdownMenuOpened)`
  min-width: 170px;
  top: unset;
  right: unset;
  cursor: initial;
  pointer-events: initial;
  padding: 10px;

  .previous-username-item {
    padding: 5px;
    font-weight: normal;
    font-size: ${ThemeFontSizeLarge};
    display: flex;
    align-items: baseline;
    justify-content: space-between;
  }
`;

interface PreviousUsernamesDropdownProps {
  previousUsernames: PreviousUsername[];
}

const PreviousUsernamesDropdown = ({ previousUsernames }: PreviousUsernamesDropdownProps) => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);

  return (
    <DropdownButton ref={buttonRef} onClick={() => setOpen((value) => !value)} type="button">
      <i className="fas fa-caret-down" />
      {open && (
        <DropdownMenu ref={menuRef}>
          {previousUsernames.map((previousUsername) => (
            <div className="previous-username-item" key={previousUsername.createdAt}>
              <span className="previous-username">{previousUsername.username}</span>
              <span
                className="previous-username-date"
                title={dateFormat(previousUsername.createdAt)}
              >
                {dayjs(previousUsername.createdAt).fromNow()}
              </span>
            </div>
          ))}
        </DropdownMenu>
      )}
    </DropdownButton>
  );
};

export default PreviousUsernamesDropdown;
