import React from 'react';
import styled from 'styled-components';
import { scrollToTop } from '../../../utils/pageScroll';
import { Panel, PanelTitle } from '../../../components/Panel';
import { ThemeHighlightStronger } from '../../../utils/ThemeNew';

const StyledChangeUsernameButton = styled.button`
  display: block;
  position: relative;
  background: ${ThemeHighlightStronger};
  color: white;
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

interface ChangeUsernameContainerProps {
  showModal: Function;
}

const ChangeUsernameContainer = ({ showModal }: ChangeUsernameContainerProps) => {
  return (
    <Panel>
      <PanelTitle title="Change Username">Change Username</PanelTitle>
      <StyledChangeUsernameButton
        onClick={() => {
          showModal();
          scrollToTop();
        }}
        type="button"
      >
        <i className="fa-solid fa-user-edit" />
        Change Username
      </StyledChangeUsernameButton>
    </Panel>
  );
};

export default ChangeUsernameContainer;
