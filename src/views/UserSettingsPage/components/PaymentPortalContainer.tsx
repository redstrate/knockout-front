import React from 'react';
import styled from 'styled-components';
import { Panel, PanelTitle } from '../../../components/Panel';
import {
  ThemeHighlightStronger,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import paymentPortal from '../../../services/paymentPortal';

const ManagePaymentsButton = styled.button`
  display: block;
  position: relative;
  background: ${ThemeHighlightStronger};
  color: white;
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

const StyledPaymentPortalDescription = styled.div`
  display: flex;
  padding: ${ThemeHorizontalPadding} ${ThemeVerticalPadding};
  padding-top: 0;
  flex-direction: row;
  flex-wrap: wrap;
`;

interface PaymentPortalContainerProps {
  returnUrl: string;
}

const PaymentPortalContainer = ({ returnUrl }: PaymentPortalContainerProps) => {
  const handleManagePaymentsClick = async (e) => {
    e.preventDefault();
    const paymentPortalUrl = (await paymentPortal(returnUrl)).url;
    window.location.replace(paymentPortalUrl);
  };

  return (
    <Panel>
      <PanelTitle title="Manage Payments">Manage Payments</PanelTitle>

      <StyledPaymentPortalDescription>
        You can manage your payment information, subscriptions, and more through this portal.
      </StyledPaymentPortalDescription>

      <ManagePaymentsButton onClick={handleManagePaymentsClick} type="button">
        <i className="fa-solid fa-dollar-sign" />
        Manage Payments
      </ManagePaymentsButton>
    </Panel>
  );
};

export default PaymentPortalContainer;
