import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { Redirect, useLocation } from 'react-router-dom';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
  ThemeFontSizeHuge,
  ThemeBackgroundLighter,
  ThemeTextColor,
  ThemeBodyBackgroundColor,
  ThemeBodyWidth,
} from '../../utils/ThemeNew';
import DeleteAccountWarning from './components/DeleteAccount';
import LoggedInOnly from '../../components/LoggedInOnly';
import UserExperienceContainer from './components/UserExperienceContainer';
import DeleteAccountContainer from './components/DeleteAccountContainer';
import ThemeConfiguratorContainer from './components/ThemeContainer';
import AvatarBackgroundPreview from './components/AvatarBackgroundPreview';
import { SubheaderLink } from '../../components/Buttons';
import {
  getKnockoutGoldCheckoutSessionId,
  getKnockoutGoldSubscription,
} from '../../services/knockoutGold';
import { pushSmartNotification } from '../../utils/notification';
import GoldContainer from './components/GoldContainer';
import getStripeClient from '../../services/stripeClient';
import { syncData } from '../../services/user';
import {
  getDonationCheckoutSessionId,
  getDonationUpgradeExpiration,
} from '../../services/donation';
import DonationContainer from './components/DonationContainer';
import PaymentPortalContainer from './components/PaymentPortalContainer';
import ChangeUsernameContainer from './components/ChangeUsernameContainer';
import ChangeUsername from './components/ChangeUsername';
import AddLoginMethodContainer from './components/AddLoginMethodContainer';

const UserSettingsPageWrapper = styled.section`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  max-width: ${ThemeBodyWidth};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  .avatar-background {
    margin-right: ${ThemeHorizontalPadding};
    @media (max-width: 900px) {
      margin-right: unset;
    }
  }

  .theme-input {
    background: ${ThemeBackgroundLighter};
    border: none;
    padding: calc(${ThemeHorizontalPadding} / 2);
  }

  .dropdown {
    position: relative;
    display: flex;
    flex-direction: row;

    color: ${ThemeTextColor};
    background: ${ThemeBodyBackgroundColor};
    transition: background 1s;
  }

  .input:last-child {
    margin-bottom: 0;
  }

  .dropdown input,
  .dropdown select {
    border: none;
    padding: 0;
    line-height: 1;
    outline: none;
    font-size: ${ThemeFontSizeMedium};
    color: inherit;
    background: ${ThemeBackgroundLighter};
    transition: background 1s;
  }

  .dropdown label {
    margin-right: ${ThemeHorizontalPadding};
  }

  .panel-desc {
    font-size: ${ThemeFontSizeSmall};
    text-align: center;
    color: ${ThemeTextColor};
  }

  .image-container {
    display: block;
    height: auto;
    margin: auto;
  }

  .input-wrapper {
    margin-top: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    align-items: center;
    background-color: ${ThemeBodyBackgroundColor};
    padding-top: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    transition: background 1s;
  }

  .input-wrapper label {
    margin-left: ${ThemeHorizontalPadding};
    font-size: ${ThemeFontSizeMedium};
  }

  .user-experience-wrapper {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .settings-column {
    display: flex;
    flex-grow: 1;
    flex-direction: column;
  }

  .settings-root {
    display: flex;
    flex-direction: row;
    @media (max-width: 900px) {
      flex-direction: column;
    }
  }

  .options-wrapper {
    padding-top: 5px;
  }

  nav.subHeader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .subHeader-buttons-root {
      display: flex;
      height: 26.6px;
      overflow: hidden;
      align-items: stretch;
      justify-content: space-between;
      width: 100%;
      text-decoration: none;
    }
  }

  .input-row {
    margin-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    align-items: center;
    background-color: ${ThemeBackgroundLighter};

    label {
      margin-left: ${ThemeHorizontalPadding};
      font-size: ${ThemeFontSizeMedium};
      flex: 0 0 250px;
    }

    .input-wrap {
      flex: 1;
      display: flex;
      border: none;
      background-color: ${ThemeBodyBackgroundColor};
      input[type='text'],
      input[type='number'],
      input[type='color'] {
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        background-color: ${ThemeBodyBackgroundColor};
        color: ${ThemeTextColor};
        border-radius: 0;
      }

      input[type='text'],
      input[type='number'] {
        flex: 1;
        border: none;
        width: 100%;
      }

      input[type='color'] {
        flex: 0 0 40px;
        height: 30px;
      }
    }
  }

  .theme-selection {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin-top: 20px;
    margin-bottom: 20px;
  }

  .theme-configuration {
    display: flex;
    flex-wrap: wrap;
    .theme-inputs {
      flex: 0 0 550px;
      box-sizing: border-box;
    }
    .theme-preview {
      flex: 1;
      .browser {
        border: 2px solid #333333;
        background: #1b1b1b;
        .tabs {
          background: #1b1b1b;
          color: white;
          display: flex;
          padding-top: 8px;
          .tab {
            margin: 0 0.4rem;
            flex: 0 1 200px;
            font-size: 14px;
            .inner {
              height: 32px;
              display: flex;
              align-items: center;
              padding: 0 0.8rem;
            }
            .title {
              flex: 1;
            }
            .action {
              flex: 0 0 24px;
              text-align: center;
            }
            &.active {
              .inner {
                background: #333333;
                border-radius: 0.4rem 0.4rem 0 0;
              }
            }
          }
        }
        .address-bar {
          background: #333333;
          color: white;
          padding: 4px;
          .inner {
            background: #2b2b2b;
            padding: 4px 8px;
            span {
              font-size: 14px;
              text-align: left;
            }
          }
        }
      }
    }
    .theme-actions {
      flex: 0 0 100%;
      padding: 0 ${ThemeHorizontalPadding};
      box-sizing: border-box;

      button {
        border: none;
        border-radius: 0;
        color: ${ThemeTextColor};
        background-color: ${ThemeBackgroundLighter};
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        margin-right: ${ThemeHorizontalPadding};
        cursor: pointer;
        i {
          margin-right: 0.5rem;
        }
      }
    }

    @media (max-width: 960px) {
      .theme-inputs,
      .theme-preview {
        flex: 0 0 100%;
      }

      .input-row {
        flex-direction: column;
        label {
          flex-basis: unset;
        }
      }
    }

    .theme-inputs {
      padding: 0 ${ThemeHorizontalPadding};
    }
  }
`;

const UserSettingsPage = () => {
  const [showDeleteAccount, setShowDeleteAccount] = useState(false);
  const [showChangeUsername, setShowChangeUsername] = useState(false);
  const [knockoutGoldSubscription, setKnockoutGoldSubscription] = useState(null);
  const [purchaseMessageShown, setPurchaseMessageShown] = useState(false);
  const [donationMessageShown, setDonationMessageShown] = useState(false);
  const [donationUpgradeExpiresAt, setDonationUpgradeExpiresAt] = useState(null);
  const { pathname, search } = useLocation();

  const loggedIn = useSelector((state) => state.user.loggedIn && state.user.username);

  const knockoutGoldPurchaseSuccess = new URLSearchParams(search).get(
    'knockoutGoldPurchaseSuccess'
  );

  const donationSuccess = new URLSearchParams(search).get('donationSuccess');

  const hasPaymentAccount =
    donationUpgradeExpiresAt || knockoutGoldSubscription?.status !== 'inactive';

  useEffect(() => {
    const fetchSubscription = async () => {
      try {
        const subscription = await getKnockoutGoldSubscription();
        if (knockoutGoldPurchaseSuccess && subscription?.status === 'active') {
          await syncData();
        }
        setKnockoutGoldSubscription(subscription);
      } catch (err) {
        console.error(err);
        setKnockoutGoldSubscription({});
      }
    };

    if (loggedIn) {
      fetchSubscription();
    }
  }, [loggedIn, knockoutGoldPurchaseSuccess]);

  useEffect(() => {
    const fetchDonationExpiration = async () => {
      try {
        const expiresAt = await getDonationUpgradeExpiration();
        setDonationUpgradeExpiresAt(expiresAt);
      } catch (err) {
        console.error(err);
        setKnockoutGoldSubscription({});
      }
    };

    if (loggedIn) {
      fetchDonationExpiration();
    }
  });

  if (!loggedIn) {
    return <Redirect to="/" />;
  }

  if (knockoutGoldPurchaseSuccess === '1' && !purchaseMessageShown) {
    pushSmartNotification({ message: 'Thank you for purchasing Knockout Gold!' });
    setPurchaseMessageShown(true);
  }

  if (donationSuccess === '1' && !donationMessageShown) {
    pushSmartNotification({ message: 'Your donation has been successfully processed!' });
    setDonationMessageShown(true);
  }

  const handleKnockoutGoldClick = async () => {
    const path = `${window.location.origin}${pathname}`;

    const stripe = await getStripeClient();
    const sessionId = await getKnockoutGoldCheckoutSessionId(
      `${path}?knockoutGoldPurchaseSuccess=1`,
      `${path}?knockoutGoldPurchaseSuccess=0`
    );
    stripe.redirectToCheckout({ sessionId });
  };

  const handleDonateClick = async () => {
    const path = `${window.location.origin}${pathname}`;

    const stripe = await getStripeClient();
    const sessionId = await getDonationCheckoutSessionId(
      `${path}?donationSuccess=1`,
      `${path}?donationSuccess=0`
    );
    stripe.redirectToCheckout({ sessionId });
  };

  return (
    <LoggedInOnly>
      <UserSettingsPageWrapper>
        <Helmet>
          <title>Settings - Knockout!</title>
        </Helmet>
        <h2>User Settings</h2>
        <nav className="subHeader">
          <span className="subHeader-buttons-root">
            <SubheaderLink to="/">
              <i className="fa-solid fa-angle-left" />
              <span>Home</span>
            </SubheaderLink>
            <SubheaderLink to="/logout">
              <i className="fa-solid fa-sign-out-alt" />
              <span>Log me out!</span>
            </SubheaderLink>
          </span>
        </nav>
        <ThemeConfiguratorContainer />
        <div className="settings-root">
          <div className="settings-column avatar-background">
            <AvatarBackgroundPreview />
            <GoldContainer
              handleSubscriptionClick={handleKnockoutGoldClick}
              subscription={knockoutGoldSubscription}
            />
            <DonationContainer
              handleDonationClick={handleDonateClick}
              donationUpgradeExpiresAt={donationUpgradeExpiresAt}
            />
          </div>
          <div className="settings-column">
            <UserExperienceContainer />
            <ChangeUsernameContainer showModal={() => setShowChangeUsername(true)} />
            {hasPaymentAccount && <PaymentPortalContainer returnUrl={window.location.href} />}
            <AddLoginMethodContainer />
            <DeleteAccountContainer showDeleteAccount={setShowDeleteAccount} />
          </div>
        </div>
      </UserSettingsPageWrapper>
      <ChangeUsername modalOpen={showChangeUsername} setModalOpen={setShowChangeUsername} />
      <DeleteAccountWarning modalOpen={showDeleteAccount} setModalOpen={setShowDeleteAccount} />
    </LoggedInOnly>
  );
};

export default UserSettingsPage;
