import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { transparentize } from 'polished';
import { useSelector } from 'react-redux';
import { ShortcodeTree } from 'shortcode-tree';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import humanizeDuration from '../../../utils/humanizeDuration';
import { ThemeFontSizeSmall, ThemeHighlightStronger } from '../../../utils/ThemeNew';
import ratingList from '../../../utils/ratingList.json';

dayjs.extend(relativeTime);

const StyledEvent = styled.div`
  display: flex;
  margin-bottom: 8px;
  padding: 5px 0;
  line-height: 1.5em;
  align-items: center;
  overflow-y: hidden;
  ${(props) => props.reply && `background: ${transparentize(0.8, ThemeHighlightStronger(props))}`};

  .event-link {
    color: #3facff;
  }

  .event-highlight {
    font-weight: 600;
    opacity: 0.7;
  }

  .event-icon {
    font-size: 24px;
    width: 35px;
    margin-right: 6px;
    text-align: center;
    display: flex;
    justify-content: center;
  }

  .rating-icon {
    width: 24px;
  }

  .event-user {
    font-weight: 600;
  }

  .event-time {
    margin-left: 8px;
    opacity: 0.5;
    font-size: ${ThemeFontSizeSmall};
    white-space: nowrap;
  }
`;

const Event = ({ event }) => {
  const currentUserId = useSelector((state) => state.user.id);
  const [banReason, setBanReason] = useState('');
  const iconRef = useRef();

  const getThread = (thread = event.data, text = thread.title, post = undefined) => (
    <Link
      className="event-link"
      to={`/thread/${thread.id}${post ? `/${post.page}#post-${post.id}` : ''}`}
    >
      {text}
    </Link>
  );
  const getUser = (user = event.data, username = user.username) => (
    <Link to={`/user/${user.id}`} className="event-user">
      <UserRoleWrapper user={user}>{username}</UserRoleWrapper>
    </Link>
  );

  const mentionsUser = (content) => {
    const tree = ShortcodeTree.parse(content);
    return tree.children.some(
      (node) =>
        node.shortcode?.name === 'quote' &&
        Number(node.shortcode?.properties?.mentionsUser) === currentUserId
    );
  };

  useEffect(() => {
    if (window.twemoji) window.twemoji.parse(iconRef.current);
    if (event.type === 'user-banned')
      setBanReason(humanizeDuration(event.data.createdAt, event.data.expiresAt));
  }, []);

  const creator = getUser(event.creator);
  let text;
  let emoji;
  let reply;
  const ratingObject = ratingList[event.content.rating];
  switch (event.type) {
    case 'thread-created':
      emoji = '📰';
      text = [creator, ' created ', getThread()];
      break;
    case 'thread-background-updated':
      emoji = '🖼️';
      text = [creator, ' updated the background of ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-deleted':
      emoji = '🗑️';
      text = [creator, ' deleted ', getThread(event.data, 'a thread')];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-moved':
      emoji = '🚚';
      text = [
        creator,
        ' moved ',
        getThread(),
        ' to ',
        <Link className="event-link" key={0} to={`subforum/${event.data.subforum.id}`}>
          {event.data.subforum.name}
        </Link>,
      ];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-pinned':
      emoji = '📌';
      text = [creator, ' pinned ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-unpinned':
      emoji = '♻️';
      text = [creator, ' unpinned ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-locked':
      emoji = '🔒';
      text = [creator, ' locked ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-unlocked':
      emoji = '🔓';
      text = [creator, ' unlocked ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-renamed':
      emoji = '✒️';
      text = [
        creator,
        ' renamed ',
        <span key={0} className="event-highlight">
          {event.content.oldTitle}
        </span>,
        ' to ',
        getThread(event.data, event.content.newTitle),
      ];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-restored':
      emoji = '🔄';
      text = [creator, ' restored ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-post-limit-reached':
      emoji = '🛑';
      text = [getThread(), ' reached its post limit and was automatically locked'];
      reply = event.data.user === currentUserId;
      break;
    case 'user-avatar-removed':
      emoji = '🔞';
      text = [creator, ' removed ', getUser(), "'s avatar because of their poor taste."];
      reply = event.data.id === currentUserId;
      break;
    case 'user-background-removed':
      emoji = '🔞';
      text = [creator, ' removed ', getUser(), "'s background because of their poor taste."];
      reply = event.data.id === currentUserId;
      break;
    case 'user-unbanned':
      emoji = '👏';
      text = [creator, ' reversed one of ', getUser(), "'s bans."];
      reply = event.data.id === currentUserId;
      break;
    case 'user-profile-removed':
      emoji = '🔞';
      text = [
        creator,
        ' removed ',
        getUser(),
        "'s profile customizations because of their poor taste.",
      ];
      reply = event.data.id === currentUserId;
      break;
    case 'user-banned':
      emoji = '⛔';
      text = [
        creator,
        ' muted ',
        getUser(event.data.user),
        event.data.post && [
          ' in ',
          getThread(event.data.thread, event.data.thread.title, event.data.post),
        ],
        ' with reason "',
        <span key={0} className="event-highlight">
          {event.data.banReason}
        </span>,
        '" for ',
        banReason,
      ];
      reply = event.data.user.id === currentUserId;
      break;
    case 'user-wiped':
      emoji = '💥';
      text = [
        creator,
        ' permanently banned and wiped ',
        getUser(event.data.user, 'a user'),
        '\'s account, with reason "',
        <span key={0} className="event-highlight">
          {event.data.banReason}
        </span>,
        '"',
      ];
      break;
    case 'post-created':
      emoji = '💬';
      text = [
        creator,
        ' created a new post in ',
        getThread(event.data.thread, event.data.thread.title, event.data),
      ];
      reply = mentionsUser(event.data.content);
      break;
    case 'rating-created':
      emoji = ratingObject && (
        <img className="rating-icon" src={ratingObject.url} alt={ratingObject.name} />
      );
      text = [
        creator,
        ' rated a post in ',
        getThread(event.data.thread, event.data.thread.title, event.data),
      ];
      reply = event.data.user === currentUserId;
      break;
    case 'profile-comment-created':
      emoji = '📝';
      text = [creator, ' commented on ', getUser(), "'s profile"];
      reply = event.data.id === currentUserId;
      break;
    case 'gold-earned':
      emoji = '🏆';
      text = [creator, ' is now a Gold Member'];
      break;
    case 'gold-lost':
      emoji = '💸';
      text = [creator, ' is no longer a Gold Member'];
      break;
    default:
      emoji = '⚠️';
      text = 'Problem retrieving event data.';
      break;
  }

  return (
    <StyledEvent reply={reply}>
      <span ref={iconRef} className="event-icon">
        {emoji}
      </span>
      <span className="event-text">{text}</span>
      <span className="event-time">{dayjs(event.createdAt).fromNow()}</span>
    </StyledEvent>
  );
};

Event.propTypes = {
  event: PropTypes.shape({
    type: PropTypes.string.isRequired,
    data: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string,
      subforum: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      }),
      user: PropTypes.shape({
        id: PropTypes.number.isRequired,
        username: PropTypes.string.isRequired,
      }),
      thread: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
      }),
      post: PropTypes.number,
      banReason: PropTypes.string,
      createdAt: PropTypes.string,
      expiresAt: PropTypes.string,
      content: PropTypes.string,
    }),
    creator: PropTypes.shape({
      id: PropTypes.number.isRequired,
      username: PropTypes.string.isRequired,
    }),
    content: PropTypes.shape({
      oldTitle: PropTypes.string,
      newTitle: PropTypes.string,
      rating: PropTypes.string,
    }),
    createdAt: PropTypes.string.isRequired,
  }).isRequired,
};

export default Event;
