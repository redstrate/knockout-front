import React from 'react';
import axios from 'axios';
import { customRender } from '../../custom_renderer';
import UserProfile from '../../../src/views/UserProfileNew';
import config from '../../../config';
import store from '../../../src/state/configureStore';
import { BASIC_USER, MODERATOR } from '../../../src/utils/roleCodes';

jest.mock('axios');

describe('UserProfile component', () => {
  const customRenderOptions = {
    path: '/user/:id',
    route: '/user/0',
  };

  const comment = {
    id: 5,
    userProfile: 0,
    author: {
      id: 2,
      username: 'Joe',
      role: { code: BASIC_USER },
      avatarUrl: '',
      backgroundUrl: '',
      createdAt: '2019-06-13T01:18:28.000Z',
      updatedAt: '2019-06-13T01:18:28.000Z',
      banned: false,
      isBanned: false,
    },
    content: 'test',
    deleted: false,
    createdAt: '2021-10-24T18:17:14.000Z',
    updatedAt: '2021-10-24T18:17:14.000Z',
  };

  beforeEach(() => {
    axios.get.mockImplementation((path) => {
      if (path.startsWith(`${config.apiHost}/user/0/posts/1`)) {
        return Promise.resolve({ data: { posts: [] } });
      }
      if (path.startsWith(`${config.apiHost}/user/0/topRatings`)) {
        return Promise.resolve({ data: [] });
      }
      if (path.startsWith(`${config.apiHost}/v2/users/0/profile/comments`)) {
        return Promise.resolve({ data: { totalComments: 1, comments: [comment], page: 1 } });
      }
      return Promise.resolve({ data: { username: 'Rando', id: 0 } });
    });
  });

  describe('as a non-logged in user', () => {
    it('hides moderator controls', async () => {
      const { findByTitle } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByTitle('Moderation', {}, { timeout: 100 })).rejects.toThrow();
    });
    it('cannot edit the profile', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
    });

    it('cannot create a comment', async () => {
      const { queryByPlaceholderText } = customRender(<UserProfile />, customRenderOptions);
      expect(queryByPlaceholderText('Add a comment')).toBeNull();
    });

    it('cannot delete a comment', async () => {
      const { queryByTitle } = customRender(<UserProfile />, customRenderOptions);
      expect(queryByTitle('Delete comment')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        id: 123,
        username: 'TestUser',
        role: { code: BASIC_USER },
        avatarUrl: 'avatar.png',
      },
    };

    beforeEach(() => {
      customRenderOptions.initialState = loggedInState;
    });

    it('hides moderator controls', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Remove Avatar', {}, { timeout: 100 })).rejects.toThrow();
    });

    it('cannot edit the profile', async () => {
      const { findByText } = customRender(<UserProfile />, customRenderOptions);
      await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
    });

    it('can create a comment', async () => {
      const { queryByPlaceholderText } = customRender(<UserProfile />, customRenderOptions);
      expect(queryByPlaceholderText('Add a comment')).not.toBeNull();
    });

    it('cannot delete a comment', async () => {
      const { queryByTitle } = customRender(<UserProfile />, customRenderOptions);
      expect(queryByTitle('Delete comment')).toBeNull();
    });

    describe('who is a comment author', () => {
      beforeEach(() => {
        customRenderOptions.initialState = {
          ...loggedInState,
          user: { ...loggedInState.user, id: 2 },
        };
      });

      it('can delete their own comment', async () => {
        const { findByTitle } = customRender(<UserProfile />, customRenderOptions);
        expect(await findByTitle('Delete comment')).not.toBeNull();
      });
    });

    describe('who is a moderator', () => {
      const modState = {
        user: {
          loggedIn: true,
          username: 'TestUser',
          id: 123,
          role: { code: MODERATOR },
        },
      };

      beforeEach(() => {
        customRenderOptions.initialState = modState;
        jest.spyOn(store, 'getState').mockReturnValue(modState);
      });

      it('shows moderator controls', async () => {
        const { findByTitle } = customRender(<UserProfile />, customRenderOptions);
        expect(await findByTitle('Moderation')).toBeTruthy();
      });

      it('cannot edit the profile', async () => {
        const { findByText } = customRender(<UserProfile />, customRenderOptions);
        await expect(findByText('Edit profile', {}, { timeout: 100 })).rejects.toThrow();
      });

      it('can create a comment', async () => {
        const { queryByPlaceholderText } = customRender(<UserProfile />, customRenderOptions);
        expect(queryByPlaceholderText('Add a comment')).not.toBeNull();
      });

      it('can delete a comment', async () => {
        const { findByTitle } = customRender(<UserProfile />, customRenderOptions);
        expect(await findByTitle('Delete comment')).not.toBeNull();
      });
    });

    describe('viewing their own profile', () => {
      const ownUserState = {
        user: {
          loggedIn: true,
          id: 0,
          username: 'Rando',
          role: { code: BASIC_USER },
          avatarUrl: 'avatar.png',
        },
      };

      beforeEach(() => {
        customRenderOptions.initialState = ownUserState;
        jest.spyOn(store, 'getState').mockReturnValue(ownUserState);
      });

      it('can edit the profile', async () => {
        const { findByText } = customRender(<UserProfile />, customRenderOptions);
        expect(await findByText('Edit profile')).toBeTruthy();
      });

      it('can create a comment', async () => {
        const { queryByPlaceholderText } = customRender(<UserProfile />, customRenderOptions);
        expect(queryByPlaceholderText('Add a comment')).not.toBeNull();
      });

      it('can delete a comment', async () => {
        const { findByTitle } = customRender(<UserProfile />, customRenderOptions);
        expect(await findByTitle('Delete comment')).not.toBeNull();
      });
    });
  });
});
