/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import axios from 'axios';
import { customRender, screen } from '../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';

import Header from '../../../src/components/Header';
import * as UserUtils from '../../../src/utils/user';
import * as EventDateUtils from '../../../src/utils/eventDates';
import { MODERATOR } from '../../../src/utils/roleCodes';
import { BAN_INFO_STORAGE_KEY } from '../../../src/utils/bannedStorage';

jest.mock('axios');

describe('Header component', () => {
  const defaultState = { mentions: { mentions: [] } };
  let getEventTextMock;
  let getEventHeaderLogoMock;
  axios.get.mockResolvedValue({ data: [] });
  beforeEach(() => {
    localStorage.clear();
    getEventHeaderLogoMock = jest
      .spyOn(EventDateUtils, 'getEventHeaderLogo')
      .mockImplementation(() => {
        return null;
      });

    getEventTextMock = jest.spyOn(EventDateUtils, 'getEventText').mockImplementation(() => {
      return 'knockout';
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('as a non-logged in user', () => {
    it('displays the knockout logo and name', () => {
      const { getAllByText } = customRender(<Header />, {
        initialState: defaultState,
      });
      expect(getAllByText('knockout!').length).toBeGreaterThanOrEqual(1);
    });

    it('provides links to important pages such as rules, calendar, search, login', () => {
      const { getByText } = customRender(<Header />, {
        initialState: defaultState,
      });
      expect(getByText('Rules').closest('a')).toHaveAttribute('href', '/rules');
      expect(getByText('Calendar').closest('a')).toHaveAttribute('href', '/calendar');
      expect(getByText('Search').closest('a')).toHaveAttribute('href', '/threadsearch');
      expect(getByText('Log in').closest('a')).toHaveAttribute('href', '/login');
    });

    it('does not have links to logged in user features, such as subscriptions and events', () => {
      customRender(<Header />, {
        initialState: defaultState,
      });

      expect(screen.queryByTitle('Subscriptions')).toBeNull();
      expect(screen.queryByText('Events')).toBeNull();
      expect(screen.queryByText('Messages')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      ...defaultState,
      user: {
        loggedIn: true,
        username: 'TestUser',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      role: { code: 'basic-user' },
      avatarUrl: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('displays links for logged in user features', () => {
      const { getByText, queryByTitle } = customRender(<Header />, {
        initialState: loggedInState,
      });

      expect(queryByTitle('Subscriptions')).not.toBeNull();
      expect(getByText('Ticker').closest('a')).toHaveAttribute('href', '/ticker');
    });

    describe('with unread subscriptions', () => {
      let checkLoginStatusMock;

      beforeEach(() => {
        checkLoginStatusMock = jest
          .spyOn(UserUtils, 'checkLoginStatus')
          .mockImplementation((_, updateHeader, __) => {
            updateHeader({
              subscriptions: [
                { id: 0, unreadPosts: 15, thread: {} },
                { id: 1, unreadPosts: 20, thread: {} },
              ],
              mentions: [],
            });
          });
      });

      afterEach(() => {
        checkLoginStatusMock.mockRestore();
      });

      it('shows me the number of unread subscriptions I currently have', async () => {
        const { queryByTitle } = customRender(<Header />, {
          initialState: loggedInState,
        });

        expect(queryByTitle('Subscriptions').querySelector('.link-notification')).toHaveTextContent(
          '2'
        );
      });
    });

    describe('with new unread mentions', () => {
      const mentionsState = {
        ...loggedInState,
        notifications: {
          notifications: [
            {
              id: 2,
              type: 'MESSAGE',
              userId: 1,
              read: false,
              createdAt: '2021-08-02T00:17:46.000Z',
              data: {
                id: 1,
                messages: [
                  {
                    id: 18,
                    conversationId: 1,
                    user: {
                      id: 3,
                      username: 'Joe',
                      role: { code: 'basic-user' },
                      avatarUrl: 'none.webp',
                    },
                  },
                ],
                users: [
                  {
                    id: 3,
                    username: 'Joe',
                    avatarUrl: 'none.webp',
                  },
                ],
              },
            },
          ],
        },
      };

      it('shows me the number of unread mentions I currently have', async () => {
        const { queryByTitle } = customRender(<Header />, {
          initialState: mentionsState,
        });

        expect(queryByTitle('Messages').querySelector('.link-notification')).toHaveTextContent('1');
      });
    });

    describe('and the user is banned', () => {
      const banInformation = {
        banMessage: 'YOU ARE BANNED!',
        expiresAt: new Date().toDateString(),
        postId: 123,
      };

      beforeEach(() => {
        localStorage.__STORE__[BAN_INFO_STORAGE_KEY] = JSON.stringify(banInformation);
      });

      it('shows me details of my ban', () => {
        const { getAllByTitle } = customRender(<Header />, {
          initialState: loggedInState,
        });

        expect(getAllByTitle('Muted')).toHaveLength(2);
      });
    });

    describe('who is a moderator', () => {
      const moderatorState = {
        mentions: { mentions: [] },
        user: {
          loggedIn: true,
          username: 'TestUser',
          role: { code: MODERATOR },
        },
      };

      it('displays a link to the moderator tools', () => {
        const { getByText } = customRender(<Header />, {
          initialState: moderatorState,
        });

        expect(getByText('Moderation').closest('a')).toHaveAttribute('href', '/moderate');
      });

      describe('with open reports', () => {
        let checkLoginStatusMock;

        beforeEach(() => {
          checkLoginStatusMock = jest
            .spyOn(UserUtils, 'checkLoginStatus')
            .mockImplementation((_, updateHeader, __) => {
              updateHeader({
                subscriptions: [],
                mentions: [],
                reports: 123,
              });
            });
        });

        afterEach(() => {
          checkLoginStatusMock.mockRestore();
        });

        it('shows me the number of open reports I currently have', async () => {
          const { getByText } = customRender(<Header />, {
            initialState: moderatorState,
          });

          expect(getByText('123')).toBeInTheDocument();
        });
      });
    });
  });
});
